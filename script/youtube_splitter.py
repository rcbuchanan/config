import re
import itertools
import subprocess


# GENERATE TRACK NUMBERS
# i=1; count=$(ls *.opus | wc -l); for f in *.opus; do tracktag --number $i --track-total $count "$f"; ((i++)); done

YEAR = "1600"
ALBUM = "Mass for 5 Voices"
ARTIST = "William Byrd"
GENRE = "Classical"
INPUT_FILE = "../mass5"
SUFFIX = "opus"
FORMAT = "TITLE - MM:SS"
times = [
    # # five voices
    "Kyrie - 0:08",
    "Gloria - 1:33",
    "Credo - 6:29",
    "Sanctus - 15:09",
    "Benedictus - 17:32",
    "Agnus Dei - 18:54",

    # # four voices
    # "Kyrie - 0:00",
    # "Gloria - 1:55",
    # "Credo - 7:52",
    # "Sanctus_Benedictus - 15:50",
    # "Agnus Dei - 19:44",
    # three voices
    # "Kyrie - 00:00",
    # "Gloria - 00:35",
    # "Credo - 05:09",
    # "Sanctus - 11:45",
    # "Agnus Dei - 14:31",
]
if FORMAT == "HH:MM:SS - TITLE":
    times = map(lambda x: re.match("([^:]*):([^:]*):([^:]*) - ([^-]*) *", x).groups(), times)
    times = map(lambda x: (x[3], int(x[0]) * 3600 + int(x[1]) * 60 + int(x[2])), times)
elif FORMAT == "TITLE - MM:SS":
    times = map(lambda x: re.match("([^-]*) - ([^:]*):([^:]*) *", x).groups(), times)
    times = map(lambda x: (x[0], int(x[1]) * 60 + int(x[2])), times)
elif FORMAT == "MM:SS - TITLE":
    times = map(lambda x: re.match("([^:]*):([^:]*) - ([^-]*) *", x).groups(), times)
    times = map(lambda x: (x[2], int(x[0]) * 60 + int(x[1])), times)
else:
    assert(False)

titles, times = zip(*times)
times = list(times)
print(times)
times = zip(range(len(titles)), titles, times, times[1:] + [None])
times = list(times)
for i, title, start, end in times:
    outfile = "{:02} {}.{}".format(i+1, title, SUFFIX)
    cmd = [
        "ffmpeg",
        "-i", "{}.{}".format(INPUT_FILE, SUFFIX),
        "-ss", str(start),
        *(["-to", str(end)] if end else []),
        "-c", "copy", outfile,
    ]
    print(cmd)
    
    subprocess.call(cmd)
    cmd = [
        "tracktag",
        "--artist", ARTIST,
        "--album", ALBUM,
        "--name", title,
        "--year", YEAR,
        "--number", str(i+1),
        "--track-total", str(len(times)),
        outfile,
    ]
    print(cmd)
    subprocess.call(cmd)
