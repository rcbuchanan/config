#!/bin/sh

# FUTURE: setup sw directory. check for WM stuff

# link bashrc stuff, ssh stuff
# for ssh: 700 for whole thing
# for gpg: 
# setup emacs
# setup misc

die () {
	echo "ERROR: $1" >&2
	exit 1
}

setup_keyboard () {
	sed -ie 's/^XKBVARIANT=.*/XKBLAYOUT="dvorak"/' /etc/default/keyboard
	sed -ie 's/^XKBOPTIONS=.*/XKBOPTIONS=",grp:shifts_toggle,ctrl:nocaps"/' /etc/default/keyboard
}

setup_ssh () {
	test -e "${HOME}/ssh/ssh" || die "${HOME}/ssh/ssh not found"
	die "not implemented!!"
}

