import imaplib
import pdb
import pickle
import collections


account = 'cliff@mail451.com'
def fixrv(s): return s[0].replace(' ', ',')

con = imaplib.IMAP4_SSL('imap.fastmail.com', 993)
con.login(account, pw)

#pdb.set_trace()

con.select('Cliff/Inbox')
with open("cliff.fastmail.p", "wb") as f:
    fetchdata = con.fetch('1:*', '(BODY[HEADER.FIELDS (SUBJECT DATE FROM)])')
    pickle.dump(fetchdata, f)
con.select('Cliff/GMAIL')
with open("cliff.gmail.p", "wb") as f:
    fetchdata = con.fetch('1:*', '(BODY[HEADER.FIELDS (SUBJECT DATE FROM)])')
    pickle.dump(fetchdata, f)

# 0. move old stuff out of inbox
#a, b = con.uid('SEARCH', 'X-GM-RAW', 'label:Inbox AND older_than:1d')
#if len(b[0]):
#    con.uid('STORE', fixrv(b), '-X-GM-LABELS', '\\Inbox')
#    con.uid('STORE', fixrv(b), '+FLAGS','\Seen')

# 1. move starred out of original folders; mark unseen so counts show up
# a, b = con.uid('SEARCH', 'X-GM-RAW', 'is:starred OR label:Days OR label:Weeks OR label:Months')
# if len(b[0]):
#     con.uid('STORE', fixrv(b), '-X-GM-LABELS', 'Days', 'Weeks', 'Months')
#     con.uid('STORE', fixrv(b), '-FLAGS','\Seen')

# a, b = con.uid('SEARCH', 'X-GM-RAW', 'is:starred AND newer_than:5d')
# if len(b[0]):
#     con.uid('STORE', fixrv(b), '+X-GM-LABELS', 'Days')

# a, b = con.uid('SEARCH', 'X-GM-RAW', 'is:starred AND older_than:5d AND newer_than:14d')
# if len(b[0]):
#     con.uid('STORE', fixrv(b), '+X-GM-LABELS', 'Weeks')

# a, b = con.uid('SEARCH', 'X-GM-RAW', 'is:starred AND older_than:14d')
# if len(b[0]):
#     con.uid('STORE', fixrv(b), '+X-GM-LABELS', 'Months')




gm = pickle.load(open("cliff.gmail.p", "rb"))[1]
fm = pickle.load(open("cliff.fastmail.p", "rb"))[1]

def froms(m):
    rec = list(filter(lambda x: type(x) == tuple, m))
    rec = list(map(lambda x: x[1].decode("ascii"), rec))
    rec = list(filter(lambda x: x.find("From: ") != -1, rec))
    rec = list(map(lambda x: x.split("\r\n"), rec))
    rec = list(map(lambda x: list(filter(
        lambda y: y.find("From: ") == 0,
        x))[0], rec))
    return rec

info = collections.Counter(froms(gm) + froms(fm))
info = list(info.items())
info.sort(key=lambda x: -x[1])
print("\n".join(map(
    "{0[1]:4} {0[0]}".format,
    info)))
