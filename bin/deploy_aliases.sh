#!/bin/sh

CONFIG="${HOME}/config"

die () { echo "$1" && exit 1; }

check_link () {
    if [ ! -e "$1" -a ! -h "$1" ] # exists, or exists as symbolic
    then
	return 1 # doesn't exist
    elif [ "$(stat -c "%N" "$1")" = "'$1' -> '$2'" ]
    then
	return 0 # correct link already in place
    else
        echo "'$1' -> '$2'"
	return 2 # conflicting file exists!
    fi
}

make_or_complain () {
    SRC="${HOME}/$1"
    TARGET="${CONFIG}/$2"
    check_link "${SRC}" "${TARGET}"
    case $? in
	1)
	    echo "creating link '${SRC}' -> '${TARGET}'"
	    ln -s "${TARGET}" "${SRC}"
	    ;;
	2)
	    echo "unexpected file at ${SRC}"
	    ;;
    esac
}

#make_or_complain ".gnupg" "../ssh/gnupg"
#make_or_complain ".ssh" "../ssh/ssh"
make_or_complain ".bashrc" "dotfiles/bashrc"
#make_or_complain ".mbsyncrc" "dotfiles/mbsyncrc"
make_or_complain ".bash_profile" "dotfiles/bash_profile"
#make_or_complain ".xinitrc" "xinitrc_no_dm"
make_or_complain ".emacs.d" "dotfiles/emacs.d"
#make_or_complain ".config/redshift.conf" "redshift"
make_or_complain ".mg" "dotfiles/mg"
#make_or_complain ".muttrc" "muttrc"
#make_or_complain ".mailcap" "mailcap"
#make_or_complain ".dosbox" "dosbox"

# fix permissions on ssh
if [ -e "${HOME}/.ssh" ]
then
    chmod -R 700 "${HOME}/.ssh"
    chmod -f 755 "${HOME}/.ssh/keys"/*.pub
fi

# fix permissions on gpg
if [ -e "${HOME}/.gnupg" ]
then
    chmod -R 700 "${HOME}/.gnupg"
    #chmod 755 "${HOME}/.gnupg/keys"/*.pub
fi

# will want to run fc-cache after this
#make_or_complain ".fonts.conf" "fonts.conf"
#make_or_complain ".fonts" "RES/fonts"
