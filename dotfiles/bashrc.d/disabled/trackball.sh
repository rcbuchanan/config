trackball () {
	IFS=$'\n' xinput list --name-only |
	while read -r dev; do
		local s="$dev"$'\n'
		case "$dev" in
		"Primax Kensington Eagle Trackball")
			xinput --set-prop "$dev" "Device Accel Constant Deceleration" 2
			xinput --set-prop "$dev" "Evdev Middle Button Emulation" 1
		;;
		"Kensington      Kensington Expert Mouse")
			xinput --set-prop "$dev" "Device Accel Constant Deceleration" 2
			xinput --set-prop "$dev" "Evdev Middle Button Emulation" 1
		;;
		*)	s="" ;;
		esac
		echo -n "$s"
	done
}
