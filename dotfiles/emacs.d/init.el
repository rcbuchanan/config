; (add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

;;
;; functions
;;
(defun insert-current-date ()
  (interactive)
  (insert (format-time-string "%Y/%m/%d\n" (current-time))))

(defun insert-current-time-stamp ()
  (interactive)
  (insert (format-time-string "%Y/%m/%d %H:%M\n" (current-time))))


;;
;; misc settings
;;

;;
;; scheme mode stuff
;;
(setq scheme-program-name "csi")

;;
;; c-mode stuff
;;
(c-add-style "openbsd"
	     '("bsd"
	       (c-backspace-function . delete-backward-char)
	       (c-syntactic-indentation-in-macros . nil)
	       (c-tab-always-indent . nil)
	       (c-hanging-braces-alist
		(block-close . c-snug-do-while))
	       (c-offsets-alist
		(arglist-cont-nonempty . *)
		(statement-cont . *))
	       (indent-tabs-mode . t)))
(setq c-default-style "openbsd")

;;
;; Autofill
;;
(add-hook 'org-mode-hook 'turn-on-flyspell)
(add-hook 'text-mode-hook 'turn-on-flyspell)


;;
;; keyboard stuff
;;
(global-set-key (kbd "M-g") 'goto-line)
(global-set-key (kbd "C-c C-d") 'insert-current-date)
(global-set-key (kbd "C-c C-t") 'insert-current-time-stamp)
(global-set-key (kbd "C-c C-w") 'count-words)

;;
;; misc settings
;;
; hide syntax coloring
;; (global-font-lock-mode 0)
(setq initial-scratch-message "")
(setq initial-major-mode 'org-mode)

(setq backup-directory-alist `(("." . "~/.saves")))

;;
;; auto-UNfill
;;
;;; Stefan Monnier <foo at acm.org>. It is the opposite of fill-paragraph    
(defun unfill-paragraph (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max))
	;; This would override `fill-column' if it's an integer.
	(emacs-lisp-docstring-fill-column t))
    (fill-paragraph nil region)))
(define-key global-map "\M-Q" 'unfill-paragraph)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#2d3743" "#ff4242" "#74af68" "#dbdb95" "#34cae2" "#008b8b" "#00ede1" "#e1e1e0"])
 '(column-number-mode t)
 '(cursor-type 'bar)
 '(custom-enabled-themes '(zenburn))
 '(custom-safe-themes
   '("e521c25ef12b83556b1055b8e49c9c33afd991eef7774519644561a963e7f4aa" default))
 '(fringe-mode 0 nil (fringe))
 '(inhibit-startup-screen t)
 '(menu-bar-mode nil)
 '(org-startup-truncated nil)
 '(scroll-bar-mode nil)
 '(tool-bar-mode nil)
 '(word-wrap t))

(setq-default fill-column 80)
(column-number-mode 1)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Iosevka Term" :foundry "UKWN" :slant normal :weight normal :height 120 :width normal)))))
